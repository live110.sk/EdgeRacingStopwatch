#include "eventdialog.h"
#include "ui_eventdialog.h"

EventDialog::EventDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventDialog)
{
    ui->setupUi(this);
}

EventDialog::~EventDialog()
{
    delete ui;
}

int EventDialog::getId()  {
    return ui->spinBox_id->value();
}

void EventDialog::setId(int id) {
    ui->spinBox_id->setValue(id);
}

 QString EventDialog::getName()  {
     return  ui->lineEdit_name->text();
}

void EventDialog::setName( QString name) {
    ui->lineEdit_name->setText(name);
}

 QDate EventDialog::getStart()  {
    return ui->dateEdit_start->date();
}

void EventDialog::setStart( QDate start) {
    ui->dateEdit_start->setDate(start);
}
