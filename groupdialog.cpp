#include <QtCore/QDateTime>
#include "groupdialog.h"
#include "ui_groupdialog.h"
#include <QDateTime>

GroupDialog::GroupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GroupDialog)
{
    ui->setupUi(this);
}

GroupDialog::~GroupDialog()
{
    delete ui;
}

 QString GroupDialog::getName()  {
    return ui->lineEdit_name->text();
}

void GroupDialog::setName( QString name) {
    ui->lineEdit_name->setText(name);
}

 QString GroupDialog::getDistance()  {
    return ui->lineEdit_distance->text();
}

void GroupDialog::setDistance( QString distance) {
    ui->lineEdit_distance->setText(distance);
}

int GroupDialog::getAgeMin()  {
    return ui->spinBox_age_min->value();
}

void GroupDialog::setAgeMin(int ageMin) {
    ui->spinBox_age_min->setValue(ageMin);
}

int GroupDialog::getAgeMax()  {
    return ui->spinBox_age_max->value();
}

void GroupDialog::setAgeMax(int ageMax) {
    ui->spinBox_age_max->setValue(ageMax);
}

bool GroupDialog::isMale()  {
    return ui->checkBox_male->isChecked();
}

void GroupDialog::setMale(bool male) {
    ui->checkBox_male->setChecked(male);
}

bool GroupDialog::isFemale()  {
    return ui->checkBox_female->isChecked();
}

void GroupDialog::setFemale(bool female) {
    ui->checkBox_female->setChecked(female);
}

int GroupDialog::getPrice()  {
    return ui->spinBox_price->value();
}

void GroupDialog::setPrice(int price) {
    ui->spinBox_price->setValue(price);
}

int GroupDialog::getId()  {
    return ui->lineEdit_id->text().toInt();
}

void GroupDialog::setId(int id) {
    ui->lineEdit_id->setText(QString::number(id));
}

void GroupDialog::setStartTime(QDateTime time) {
    ui->dateTimeEdit_start->setDateTime(time);

}

void GroupDialog::setComment(QString qString) {
    ui->plainTextEdit_comment->setPlainText(qString);

}

QDateTime GroupDialog::getStartTime() {
    return ui->dateTimeEdit_start->dateTime();

}

QString GroupDialog::getComment() {
    return ui->plainTextEdit_comment->toPlainText();

}
