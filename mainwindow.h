#pragma once

#include <QMainWindow>
#include <QtWidgets/QTextBrowser>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlRelationalDelegate>
#include <QtWidgets/QTableView>
#include <QtSerialPort/QSerialPort>
#include "TableModelChip.h"
#include "TableModelDistance.h"
#include "settingsdialog.h"
#include "chipdialog.h"
#include "TableModelParticipant.h"
#include "TableModelTeam.h"
#include "TableModelGroup.h"
#include "TableModelEvent.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

    static void messageToFile(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    enum tabName {
        Splits = 0, Requests = 1, Results = 2, Distances = 3, Events = 4, Groups = 5, Teams = 6, Participants = 7, Chips = 8, Debug=1, Reader = 0
    };

private slots:

    void on_tableView_split_doubleClicked(QModelIndex index);

    void on_tableView_request_doubleClicked(QModelIndex index);

    void on_tableView_event_doubleClicked(QModelIndex index);

    void on_tableView_group_doubleClicked(QModelIndex index);

    void on_actionExit_triggered();

    void on_actionApply_triggered();

    void on_actionReload_triggered();

    void on_actionAdd_Record_triggered();

    void on_actionSplit_triggered();

    void on_actionRequest_triggered();

    void on_actionProtocol_triggered();

    void on_actionDistance_triggered();

    void on_actionEvent_triggered();

    void on_actionGroup_triggered();

    void on_actionTeam_triggered();

    void on_actionParticipant_triggered();

    void on_actionChip_triggered();

    void on_actionChip_Inventarization_triggered();

    void on_tableView_team_doubleClicked(const QModelIndex &index);

    void on_tableView_chip_doubleClicked(const QModelIndex &index);

    void on_tableView_distance_doubleClicked(const QModelIndex &index);

    void on_tableView_participant_doubleClicked(const QModelIndex &index);

    QString on_actionDatabase_triggered();

    void on_action_Chip_Inventarization_toggled(bool arg1);

    void on_action_Settings_triggered();

    void on_actionDisconnect_triggered();

    void on_actionConnect_triggered();

    void readData();

    void on_action_test_chip_code_triggered();

    void on_actionDebug_triggered();

    void on_actionReader_triggered();

private:
    Ui::MainWindow *ui;

    bool prepareDebugToFileOutput();

    void openSqlDatabase();

    QSqlTableModel *model;

    void setupModel(const QString &tableName, const QStringList &headers);

    void createUI();

    TableModelTeam *modelTeam;
    TableModelEvent *modelEvent;
    TableModelDistance *modelDistance;
    QSqlQueryModel *modelRequest;
    QSqlTableModel *modelSplit;
    TableModelParticipant *modelParticipant;
    TableModelGroup *modelGroup;
    TableModelChip *modelChip;

    QSqlTableModel *getCurrentTabModel();

    QSqlRelationalDelegate *delegateTabRequest;

    SettingsDialog *m_settings = nullptr;
    QSerialPort *m_serial = nullptr;

    void setBasicTableViewProperties(QTableView *tableView, QAbstractItemModel *itemModel, QAbstractItemView::EditTrigger editTrigger);

    QString serialData;
    unsigned long serialDataBegan;

    void processChipCode(QString chipCode);


    QString currentChipCode;
    int sameChipCodeCount;
    uint64_t startTimeReadData;

    ChipDialog *chipDialog = nullptr;

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;
};

