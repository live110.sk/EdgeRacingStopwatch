//
// Created by livello on 21.09.18.
//

#include "Distance.h"

const QString &Distance::getId() const {
    return id;
}

void Distance::setId(const QString &id) {
    Distance::id = id;
}

const QString &Distance::getName() const {
    return name;
}

void Distance::setName(const QString &name) {
    Distance::name = name;
}

const QString &Distance::getNumber_from() const {
    return number_from;
}

void Distance::setNumber_from(const QString &number_from) {
    Distance::number_from = number_from;
}

const QString &Distance::getNumber_max() const {
    return number_max;
}

void Distance::setNumber_max(const QString &number_max) {
    Distance::number_max = number_max;
}

const QString &Distance::getLaps_limit() const {
    return laps_limit;
}

void Distance::setLaps_limit(const QString &laps_limit) {
    Distance::laps_limit = laps_limit;
}

const QTime &Distance::getTime_limit() const {
    return time_limit;
}

void Distance::setTime_limit(const QTime &time_limit) {
    Distance::time_limit = time_limit;
}

const QString &Distance::getLap_length() const {
    return lap_length;
}

void Distance::setLap_length(const QString &lap_length) {
    Distance::lap_length = lap_length;
}

const QString &Distance::getLap_climb() const {
    return lap_climb;
}

void Distance::setLap_climb(const QString &lap_climb) {
    Distance::lap_climb = lap_climb;
}

const QDateTime &Distance::getStart_time() const {
    return start_time;
}

void Distance::setStart_time(const QDateTime &start_time) {
    Distance::start_time = start_time;
}
