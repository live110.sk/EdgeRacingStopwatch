#include "distancedialog.h"
#include "ui_distancedialog.h"

DistanceDialog::DistanceDialog(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DistanceDialog) {
    ui->setupUi(this);
    setModal(true);
}

DistanceDialog::~DistanceDialog() {
    delete ui;
}

void DistanceDialog::setID(QString value) {
    ui->lineEdit_id->setText(value);
}

void DistanceDialog::setName(QString value) {
    ui->lineEdit_name->setText(value);
}

void DistanceDialog::setLapLimit(QString value) {
    ui->lineEdit_laps->setText(value);
}

void DistanceDialog::setTimeLimit(int value) {
    ui->spinBox_time_limit->setValue(value);
}

void DistanceDialog::setLapLength(QString value) {
    ui->lineEdit_length->setText(value);
}

void DistanceDialog::setLapClimb(QString value) {
    ui->lineEdit_climb->setText(value);
}

void DistanceDialog::setNumberFrom(QString value) {
    ui->lineEdit_number_from->setText(value);
}

void DistanceDialog::setNumberMax(QString value) {
    ui->lineEdit_number_max->setText(value);
}
void DistanceDialog::setComment(QString value) {
    ui->textEdit_comment->setText(value);
}

QString  DistanceDialog::getID(){
    return ui->lineEdit_id->text();
}
QString  DistanceDialog::getName(){
    return ui->lineEdit_name->text();
}
QString  DistanceDialog::getLapLimit(){
    return ui->lineEdit_laps->text();
}

int  DistanceDialog::getTimeLimit(){
    return ui->spinBox_time_limit->value();
}
QString  DistanceDialog::getLapLength(){
    return ui->lineEdit_length->text();
}
QString  DistanceDialog::getLapClimb(){
    return ui->lineEdit_climb->text();
}
QString  DistanceDialog::getNumberFrom(){
    return ui->lineEdit_number_from->text();
}
QString  DistanceDialog::getNumberMax(){
    return ui->lineEdit_number_max->text();
}
QString  DistanceDialog::getComment(){
    return QString(ui->textEdit_comment->toPlainText());
}
