#pragma once

#include <QDialog>
#include <QDateTime>

namespace Ui {
class ParticipantDialog;
}

class ParticipantDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ParticipantDialog(QWidget *parent = nullptr);
    ~ParticipantDialog();

private:
    Ui::ParticipantDialog *ui;
public:
    int getId() ;

    void setId(int id);

     QString getFirstName() ;

    void setFirstName( QString firstName);

     QString getSecondName() ;

    void setSecondName( QString secondName);

     QString getThirdName() ;

    void setThirdName( QString thirdName);

     QString getCity() ;

    void setCity( QString city);

    int getBirthYear() ;

    void setBirthYear(int birthYear);

     QString getPersonalMobilePhone() ;

    void setPersonalMobilePhone( QString personalMobilePhone);

    QString getFriendMobilePhone();

    void setFriendMobilePhone(QString friendMobilePhone);

    QString getEmail();

    void setEmail(QString email);

    QDateTime getCreationDateTime() ;

    void setCreationDateTime(QDateTime creationDateTime);

    int getTeamID();

    void setTeamID(int teamID);

    int getRating();

    void setRating(int rating);

    QString  getComment();

    void setComment(QString comment);
};

