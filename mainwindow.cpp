#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_teamdialog.h"
#include "teamdialog.h"
#include "chipdialog.h"
#include "distancedialog.h"
#include "ui_chipdialog.h"
#include "TableModelDistance.h"
#include "settingsdialog.h"
#include <QDebug>
#include <QtCore/QFile>
#include <QtCore/QDateTime>
#include <QtSql/QSqlError>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRelationalDelegate>
#include <QtWidgets/QFileDialog>
#include "QMessageBox"
#include "participantdialog.h"
#include "HorizontalTabStyle.h"
#include "eventdialog.h"

QFile *debugFile;
QTextStream *textStream;
QTextBrowser *textBrowser_debug;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    textBrowser_debug = ui->textBrowser_debug;
    ui->statusBar->showMessage(tr("File"));
    if (prepareDebugToFileOutput()) {
        qInstallMessageHandler(messageToFile);
        qDebug() << "Log file opened successfull!";
    }
    qInfo() << "QT_COMPILE_VERSION=" << QT_VERSION_MAJOR << "." << QT_VERSION_MINOR;


    QFontMetrics m(ui->textBrowser_debug->font());
    int RowHeight = m.lineSpacing();
    ui->textBrowser_debug->setFixedHeight(5 * RowHeight);
    ui->tabWidget_bottom->setFixedHeight(5 * RowHeight);
//    ui->tableWidget_participant.

    openSqlDatabase();
    QObject::connect(ui->tableView_team, SIGNAL(entered(
                                                        const QModelIndex&)), this, SLOT(on_tableView_team_doubleClicked(
                                                                                                 const QModelIndex &)));
    QObject::connect(ui->tableView_chip, SIGNAL(entered(
                                                        const QModelIndex&)), this, SLOT(on_tableView_chip_doubleClicked(
                                                                                                 const QModelIndex &)));
    QObject::connect(ui->tableView_distance, SIGNAL(entered(
                                                            const QModelIndex&)), this, SLOT(on_tableView_distance_doubleClicked(
                                                                                                     const QModelIndex &)));

    QObject::connect(ui->tableView_participant, SIGNAL(entered(
                                                               const QModelIndex&)), this, SLOT(on_tableView_participant_doubleClicked(
                                                                                                        const QModelIndex &)));

    QObject::connect(ui->tableView_event, SIGNAL(entered(
                                                         const QModelIndex&)), this, SLOT(on_tableView_event_doubleClicked(
                                                                                                  const QModelIndex &)));


    QObject::connect(ui->action_Database, SIGNAL(triggered()), this, SLOT(on_actionDatabase_triggered()));

    QObject::connect(ui->actionAboutQt, &QAction::triggered, this, &QApplication::aboutQt);

    QObject::connect(ui->actionTest_chip_code, SIGNAL(triggered()), this, SLOT(on_action_test_chip_code_triggered()));

    m_settings = new SettingsDialog;
    m_serial = new QSerialPort(this);
    ui->actionConnect->setEnabled(false);
    ui->actionDisconnect->setEnabled(false);
    chipDialog = new ChipDialog(this);
    ui->tabWidget_bottom->tabBar()->setStyle(new HorizontalTabStyle);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_actionExit_triggered() {
    qDebug() << tr("File");

}

void MainWindow::messageToFile(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QString msgType;
    switch (type) {
        case QtDebugMsg:
            msgType = tr("<Debug> ");
            break;
        case QtWarningMsg:
            msgType = tr("<Warning> ");
            break;
        case QtCriticalMsg:
            msgType = tr("<Critical> ");
            break;
        case QtInfoMsg:
            msgType = tr("<Info> ");
            break;
        case QtFatalMsg:
            msgType = tr("<Fatal> ");
            break;

    }
    QString fullMsg;
    fullMsg.sprintf("%s %s %s, %s:%u", QDateTime::currentDateTime().toString("dd.MM.yy hh:mm:ss").toLocal8Bit().data(),
                    msgType.toLocal8Bit().data(), msg.toLocal8Bit().constData(), context.function, context.line);
    *textStream << fullMsg << endl;
    fprintf(stderr, "%s \n", fullMsg.toLocal8Bit().constData());
    textBrowser_debug->append(fullMsg);
}

bool MainWindow::prepareDebugToFileOutput() {
    debugFile = new QFile("protocol.log");
    if (!debugFile->open(QIODevice::ReadWrite | QIODevice::Text)) {
        qWarning() << "Can not open protocol.log for writing!!!";
        return false;
    }
    textStream = new QTextStream(debugFile);
    return true;
}

void MainWindow::openSqlDatabase() {
    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    QFile *dbFile = new QFile("/home/livello/PROG/EdgeRacingStopwatch/storage.sqlite");
    if (dbFile->exists())
        sdb.setDatabaseName("/home/livello/PROG/EdgeRacingStopwatch/storage.sqlite");
    while (!sdb.open()) {
        qCritical() << "Can not open storage.sqlite " << sdb.lastError().text();
        sdb.setDatabaseName(on_actionDatabase_triggered());
    }
    qInfo() << tr("database opened ") << sdb.databaseName();
    modelParticipant = new TableModelParticipant(this, sdb, "participant");
    setBasicTableViewProperties(ui->tableView_participant, modelParticipant, QAbstractItemView::NoEditTriggers);

    modelSplit = new QSqlTableModel(this, sdb);
    modelSplit->setTable("split");
    modelSplit->setEditStrategy(QSqlTableModel::OnManualSubmit);
    modelSplit->select();
    setBasicTableViewProperties(ui->tableView_split, modelSplit, QAbstractItemView::NoEditTriggers);


//    modelRequest = new QSqlRelationalTableModel(this, sdb);
//    modelRequest->setTable("request");
//    modelRequest->setRelation(2, QSqlRelation("team", "id", "team_name"));
//    modelRequest->setRelation(3, QSqlRelation("groups", "id", "group_name"));
//    modelRequest->setRelation(4, QSqlRelation("event", "id", "event_name"));
//    modelRequest->setRelation(6, QSqlRelation("participant", "id", "first_name,second_name,third_name"));
    modelRequest = new QSqlQueryModel();
    QString request = "SELECT R.id, E.event_name, P.first_name, P.second_name, P.third_name, T.team_name, G.group_name,R.creation_datetime, R.start_number,R.chip_number,R.approved,R.is_finished,R.paid_amount,R.comment\n"
                      "FROM request R\n"
                      "       INNER JOIN team T ON R.team_id = T.id\n"
                      "       INNER JOIN groups G ON R.group_id = G.id\n"
                      "       INNER JOIN event E ON R.event_id = E.id\n"
                      "       INNER JOIN participant P ON R.participant_id = P.id";
    modelRequest->setQuery(request, sdb);
//    modelRequest->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    modelRequest->select();
//    delegateTabRequest = new QSqlRelationalDelegate(ui->tableView_request);
//    ui->tableView_request->setItemDelegate(delegateTabRequest);
//    setBasicTableViewProperties(ui->tableView_request, modelRequest, QAbstractItemView::AllEditTriggers);

    ui->tableView_request->setModel(modelRequest);
    ui->tableView_request->setSortingEnabled(true);
    ui->tableView_request->show();
    ui->tableView_request->setEditTriggers(QAbstractItemView::AllEditTriggers);
    ui->tableView_request->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_request->installEventFilter(this);


    modelDistance = new TableModelDistance(this, sdb, "distance");
    setBasicTableViewProperties(ui->tableView_distance, modelDistance, QAbstractItemView::NoEditTriggers);

    modelEvent = new TableModelEvent(this, sdb, "event");
    setBasicTableViewProperties(ui->tableView_event, modelEvent, QAbstractItemView::NoEditTriggers);

    modelChip = new TableModelChip(this, sdb, "chip");
    setBasicTableViewProperties(ui->tableView_chip, modelChip, QAbstractItemView::NoEditTriggers);

    modelGroup = new TableModelGroup(this, sdb, "groups");
    modelGroup->setRelation(2, QSqlRelation("distance", "id", "distance_name"));
    QSqlRelationalDelegate *delegateTabGroup = new QSqlRelationalDelegate(ui->tableView_request);
    ui->tableView_group1->setItemDelegate(delegateTabGroup);
    setBasicTableViewProperties(ui->tableView_group1, modelGroup, QAbstractItemView::AllEditTriggers);

    modelTeam = new TableModelTeam(this, sdb, "team");
    setBasicTableViewProperties(ui->tableView_team, modelTeam, QAbstractItemView::NoEditTriggers);

}

void MainWindow::setBasicTableViewProperties(QTableView *tableView, QAbstractItemModel *itemModel, QAbstractItemView::EditTrigger editTrigger) {
    tableView->setModel(itemModel);
    tableView->setSortingEnabled(true);
    tableView->show();
    tableView->setEditTriggers(editTrigger);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->installEventFilter(this);
}

void MainWindow::on_actionApply_triggered() {
    getCurrentTabModel()->submitAll();
//    delegateTabRequest.
}

void MainWindow::on_actionReload_triggered() {
    getCurrentTabModel()->revertAll();

}

void MainWindow::on_actionAdd_Record_triggered() {
    if (ui->tabWidget->currentIndex() == tabName::Requests) {
        modelRequest->insertRows(0, 1);
    } else
        getCurrentTabModel()->insertRows(0, 1);

    switch (ui->tabWidget->currentIndex()) {
        case tabName::Teams:
            on_tableView_team_doubleClicked(getCurrentTabModel()->index(0, 0));
            break;
        case tabName::Chips:
            on_tableView_chip_doubleClicked(getCurrentTabModel()->index(0, 0));
        case tabName::Distances:
            on_tableView_distance_doubleClicked(getCurrentTabModel()->index(0, 0));
    }
}

QSqlTableModel *MainWindow::getCurrentTabModel() {
    switch (ui->tabWidget->currentIndex()) {
        case tabName::Splits:
            return modelSplit;
        case tabName::Requests:
            return dynamic_cast<QSqlTableModel *>(modelRequest);
        case tabName::Results:
            qDebug() << "Can not add column!!!";
        case tabName::Distances:
            return modelDistance;
        case tabName::Events:
            return modelEvent;
        case tabName::Groups:
            return modelGroup;
        case tabName::Teams:
            return modelTeam;
        case tabName::Participants:
            return modelParticipant;
        case tabName::Chips:
            return modelChip;
    }
    qDebug() << "Can not add column!!!";
    return modelSplit;

}

void MainWindow::on_actionSplit_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Splits);

}

void MainWindow::on_actionRequest_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Requests);
}

void MainWindow::on_actionProtocol_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Results);
}

void MainWindow::on_actionDistance_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Distances);
}

void MainWindow::on_actionEvent_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Events);
}

void MainWindow::on_actionGroup_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Groups);
}

void MainWindow::on_actionTeam_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Teams);
}

void MainWindow::on_actionParticipant_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Participants);
}

void MainWindow::on_actionChip_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Chips);
}

void MainWindow::on_actionChip_Inventarization_triggered() {
    ui->tabWidget->setCurrentIndex(tabName::Chips);
}

void MainWindow::on_tableView_team_doubleClicked(const QModelIndex &index) {
    TeamDialog *teamDialog = new TeamDialog(this);
    modelTeam->initDialog(teamDialog, index);
    teamDialog->exec();
    if (teamDialog->result()) {
        modelTeam->applyDialog(teamDialog, index);
    } else
        on_actionReload_triggered();
}

void MainWindow::on_tableView_chip_doubleClicked(const QModelIndex &index) {
    chipDialog->setModal(false);
    modelChip->initDialog(chipDialog, index);
    chipDialog->exec();
    if (chipDialog->result()) {
        modelChip->applyDialog(chipDialog, index);
    } else
        on_actionReload_triggered();
}

void MainWindow::on_tableView_distance_doubleClicked(const QModelIndex &index) {
    DistanceDialog *distanceDialog = new DistanceDialog(this);
    modelDistance->initDialog(distanceDialog, index);
    distanceDialog->exec();
    if (distanceDialog->result()) {
        modelDistance->applyDialog(distanceDialog, index);
    } else
        on_actionReload_triggered();


}

QString MainWindow::on_actionDatabase_triggered() {
    QFileDialog qfileDialog;
    qfileDialog.setOption(QFileDialog::Option::DontConfirmOverwrite, true);
    return QString(qfileDialog.getOpenFileName(this,
                                               tr("Test File for writing"), QDir::currentPath()));
}

void MainWindow::on_action_Chip_Inventarization_toggled(bool arg1) {
    qDebug() << "Toggled Inventarization:" << arg1;


}

void MainWindow::on_action_Settings_triggered() {
    m_settings->setModal(true);
    m_settings->show();
    ui->actionConnect->setEnabled(true);
    ui->actionConnect->setText(tr("&Connect") + " " + m_settings->settings().name);
}

void MainWindow::on_actionDisconnect_triggered() {
    if (m_serial->isOpen())
        m_serial->close();
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->action_Settings->setEnabled(true);

}

void MainWindow::readData() {
    const QByteArray data = m_serial->readAll();

    if (data.length() == 0)
        return;

    uint64_t nowTime = QDateTime::currentDateTime().toMSecsSinceEpoch();
    if (serialData.length() == 0 || nowTime - startTimeReadData > 300) {
        serialData = QString(data.toHex());
        startTimeReadData = nowTime;
    } else
        serialData = serialData + QString(data.toHex());
    if (serialData.length() >= 6) {
        disconnect(m_serial, &QSerialPort::readyRead, 0, 0);
        serialData.truncate(6);
        ui->textBrowser_reader->append(QDateTime::currentDateTime().toString("hh:mm:ss:zzz ") + serialData);
        processChipCode(serialData);
        serialData = "";
        connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    }

}

void MainWindow::on_actionConnect_triggered() {
    ui->actionConnect->setEnabled(false);
    ui->actionDisconnect->setEnabled(true);
    ui->action_Settings->setEnabled(false);
    const SettingsDialog::Settings p = m_settings->settings();
    m_serial->setPortName(p.name);
    m_serial->setBaudRate(p.baudRate);
    m_serial->setDataBits(p.dataBits);
    m_serial->setParity(p.parity);
    m_serial->setStopBits(p.stopBits);
    m_serial->setFlowControl(p.flowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
        qDebug() << (tr("Connected to %1 : %2, %3, %4, %5, %6")
                .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
        connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    } else {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());

        qDebug() << (tr("Open error:")) << m_serial->errorString();
    }

}

void MainWindow::processChipCode(QString chipCode) {
    if (ui->action_Chip_Inventarization->isChecked()) {
        if (chipCode.compare(currentChipCode) == 0) {
            sameChipCodeCount++;
            if (sameChipCodeCount < m_settings->settings().sameChipCodeCount)
                return;
        } else {
            currentChipCode = QString(chipCode);
            sameChipCodeCount = 1;
            return;
        }
        sameChipCodeCount = 0;
        ui->tabWidget->setCurrentIndex(tabName::Chips);
        int row = modelChip->findChipCode(chipCode);
        if (row >= 0) {
            modelChip->selectRow(row);
            getCurrentTabModel()->setData(getCurrentTabModel()->index(row, TableModelChip::columns::CREATION_DATETIME), QDateTime::currentDateTime());
            on_tableView_chip_doubleClicked(modelChip->index(row, TableModelChip::columns::CHIP_CODE));

        } else {
            getCurrentTabModel()->insertRows(0, 1);
            getCurrentTabModel()->setData(getCurrentTabModel()->index(0, TableModelChip::columns::IS_ACTIVE), true);
            getCurrentTabModel()->setData(getCurrentTabModel()->index(0, TableModelChip::columns::CREATION_DATETIME), QDateTime::currentDateTime());
            getCurrentTabModel()->setData(getCurrentTabModel()->index(0, TableModelChip::columns::CHIP_CODE), chipCode);
            on_tableView_chip_doubleClicked(modelChip->index(0, 0));
        }
    }

}

void MainWindow::on_action_test_chip_code_triggered() {
    qDebug() << "process test code";
    processChipCode("testcode");
}

void MainWindow::on_tableView_participant_doubleClicked(const QModelIndex &index) {
    ParticipantDialog *participantDialog = new ParticipantDialog(this);
//    participantDialog->show();
    modelParticipant->initDialog(participantDialog, index);
    participantDialog->exec();
    if (participantDialog->result()) {
        modelParticipant->applyDialog(participantDialog, index);
    } else
        on_actionReload_triggered();

}

bool MainWindow::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return) {
            switch (ui->tabWidget->currentIndex()) {
                case tabName::Chips:
                    on_tableView_chip_doubleClicked(ui->tableView_chip->currentIndex());
                    break;
                case tabName::Distances:
                    on_tableView_distance_doubleClicked(ui->tableView_distance->currentIndex());
                    break;
                case tabName::Teams:
                    on_tableView_team_doubleClicked(ui->tableView_team->currentIndex());
                    break;
                case tabName::Participants:
                    on_tableView_participant_doubleClicked(ui->tableView_participant->currentIndex());
                    break;
                case tabName::Splits:
                    on_tableView_split_doubleClicked(ui->tableView_split->currentIndex());
                    break;
                case tabName::Requests:
                    on_tableView_request_doubleClicked(ui->tableView_request->currentIndex());
                    break;
                case tabName::Results:
                    break;
                case tabName::Events:
                    on_tableView_event_doubleClicked(ui->tableView_event->currentIndex());
                    break;
                case tabName::Groups:
                    on_tableView_group_doubleClicked(ui->tableView_group1->currentIndex());
                    break;
            }


            return true;
        }
    }
    return QObject::eventFilter(obj, event);
}

void MainWindow::on_tableView_split_doubleClicked(QModelIndex index) {

}

void MainWindow::on_tableView_request_doubleClicked(QModelIndex index) {

}

void MainWindow::on_tableView_event_doubleClicked(QModelIndex index) {
    EventDialog *eventDialog = new EventDialog(this);
    modelEvent->initDialog(eventDialog, index);
    eventDialog->exec();
    if (eventDialog->result()) {
        modelEvent->applyDialog(eventDialog, index);
    } else
        on_actionReload_triggered();

}

void MainWindow::on_tableView_group_doubleClicked(QModelIndex index) {
    GroupDialog *groupDialog = new GroupDialog(this);
    modelGroup->initDialog(groupDialog, index);
    groupDialog->exec();
    if (groupDialog->result()) {
        modelGroup->applyDialog(groupDialog, index);
    } else
        on_actionReload_triggered();

}

void MainWindow::on_actionDebug_triggered() {
    ui->tabWidget_bottom->setCurrentIndex(tabName::Debug);

}

void MainWindow::on_actionReader_triggered() {
    ui->tabWidget_bottom->setCurrentIndex(tabName::Reader);
}
