#ifndef CHIPDIALOG_H
#define CHIPDIALOG_H

#include <QDialog>

namespace Ui {
class ChipDialog;
}

class ChipDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChipDialog(QWidget *parent = nullptr);
    ~ChipDialog();
    Ui::ChipDialog * getUI();
    Ui::ChipDialog *ui;

    void setID(QString i);

    QString getNumber();

    QString getCode();

    bool getActive();

    QDateTime getCreated();

    QString getComment();

    void setNumber(QString value);
    void setCode(QString value);
    void setComment(QString value);
    void setActive(bool value);
    void setCreated(QDateTime value);
};

#endif // CHIPDIALOG_H
