#include <QtSql/QSqlTableModel>
#include <QtCore/QDir>
#include <QtWidgets/QFileDialog>
#include "TableModelEvent.h"
#include "QTime"
#include "QDebug"
#include "eventdialog.h"

TableModelEvent::TableModelEvent(QObject *parent, const QSqlDatabase &db, const QString dbTable) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

void TableModelEvent::initDialog(EventDialog *dialog,QModelIndex index) {
    int row = index.row();
    dialog->setId(index.sibling(row, columns::ID).data().toInt());
    dialog->setName(index.sibling(row, columns::NAME).data().toString());
    dialog->setStart(index.sibling(row, columns::START_DATE).data().toDate());
}

void TableModelEvent::applyDialog(EventDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::NAME),dialog->getName());
    setData(this->index(row, columns::START_DATE),dialog->getStart());
    this->submitAll();
}


