#pragma once


#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include "distancedialog.h"
#include "participantdialog.h"

class TableModelParticipant : public QSqlTableModel {
public:
    enum columns {
        ID = 0, FIRST_NAME = 1, SECOND_NAME = 2, THIRD_NAME = 3, BIRTH_YEAR = 4, CITY = 5, PERSONAL_MOBILE_PHONE = 6, FRIEND_MOBILE_PHONE = 7, EMAIL = 8, CREATION_DATETIME = 9, TEAM_ID = 10, RATING = 11, COMMENT = 12
    };

    TableModelParticipant(QObject *parent, const QSqlDatabase &db, const QString dbTable);

    void initDialog(ParticipantDialog *dialog, QModelIndex index);

    void applyDialog(ParticipantDialog *dialog, QModelIndex index);

    QString on_actionDatabase_triggered();


};


