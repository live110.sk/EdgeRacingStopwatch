#pragma once


#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include "distancedialog.h"

class TableModelDistance : public QSqlTableModel {
public:
    enum columns {
        ID = 0, NAME = 1, NUMBER_FROM = 2, NUMBER_MAX = 3, LAP_LIMIT = 4, TIME_LIMIT = 5, LAP_LENGTH = 6, LAP_CLIMB = 7, START_TIME = 8, COMMENT = 9
    };

    TableModelDistance(QObject *parent, const QSqlDatabase &db, const QString dbTable);
    void initDialog(DistanceDialog *dialog, QModelIndex index);
    void applyDialog(DistanceDialog *dialog, QModelIndex index);



};


