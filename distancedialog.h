#pragma once

#include <QDialog>

namespace Ui {
class DistanceDialog;
}

class DistanceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DistanceDialog(QWidget *parent = nullptr);
    ~DistanceDialog();
    void setID(QString value);
    void setName(QString value);
    void setLapLimit(QString value);
    void setTimeLimit(int value);
    void setLapLength(QString value);
    void setLapClimb(QString value);
    void setStartTime(QDateTime value);
    void setNumberFrom(QString value);
    void setNumberMax(QString value);
    void setComment(QString value);
    QString  getID();
    QString  getName();
    QString  getLapLimit();
    int getTimeLimit();
    QString  getLapLength();
    QString  getLapClimb();
    QDateTime getStartTime();
    QString  getNumberFrom();
    QString  getNumberMax();
    QString  getComment();


private:
    Ui::DistanceDialog *ui;


};
