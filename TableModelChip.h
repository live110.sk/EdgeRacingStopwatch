#pragma once


#include <QtSql/QSqlTableModel>
#include "distancedialog.h"
#include "chipdialog.h"

enum column {
    ID = 0, NAME = 1, NUMBER_FROM = 2, NUMBER_MAX = 3, LAP_LIMIT = 4, TIME_LIMIT = 5, LAP_LENGTH = 6, LAceP_CLIMB = 7, START_TIME = 8
};
class TableModelChip: public QSqlTableModel {
public:
    enum columns {
        ID = 0, CHIP_CODE = 1, NUMBER = 2, CREATION_DATETIME = 3, IS_ACTIVE = 4, COMMENT = 5};
    TableModelChip(QObject *parent, const QSqlDatabase &db,const QString dbTable);
    int findChipCode(QString chipCode);

    void initDialog(ChipDialog *dialog, QModelIndex index);
    void applyDialog(ChipDialog *dialog, QModelIndex index);
};