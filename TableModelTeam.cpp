#include "TableModelTeam.h"
#include "QDebug"
#include "QtSql/QSqlRecord"
#include "teamdialog.h"
#include <QDateTime>


TableModelTeam::TableModelTeam(QObject *parent, const QSqlDatabase &db, const QString dbTable) : QSqlTableModel(parent, db) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

int TableModelTeam::findTeamCode(QString TeamCode) {
    for (int row = 0; row < rowCount(); row++) {
        QString tableTeamCode = this->record(row).value("id").toString();

        if (tableTeamCode.compare(TeamCode, Qt::CaseInsensitive) == 0) {
            return row;
        }
    }
    return -1;
}

void TableModelTeam::initDialog(TeamDialog *dialog, QModelIndex index) {
    int row = index.row();
    dialog->setId(index.sibling(row, columns::ID).data().toString());
    dialog->setName(index.sibling(row, columns::NAME).data().toString());
    dialog->setCity(index.sibling(row, columns::CITY).data().toString());
}

void TableModelTeam::applyDialog(TeamDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::NAME),dialog->getName());
    setData(this->index(row, columns::CITY),dialog->getCity());
    submitAll();
}
