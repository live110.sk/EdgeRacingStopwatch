#include <QtSql/QSqlTableModel>
#include <QtCore/QDir>
#include <QtWidgets/QFileDialog>
#include "TableModelParticipant.h"
#include "QTime"
#include "QDebug"
#include "participantdialog.h"

TableModelParticipant::TableModelParticipant(QObject *parent, const QSqlDatabase &db, const QString dbTable) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

void TableModelParticipant::initDialog(ParticipantDialog *dialog,QModelIndex index) {
    int row = index.row();
    dialog->setId(index.sibling(row, columns::ID).data().toInt());
    dialog->setFirstName(index.sibling(row, columns::FIRST_NAME).data().toString());
    dialog->setSecondName(index.sibling(row, columns::SECOND_NAME).data().toString());
    dialog->setThirdName(index.sibling(row, columns::THIRD_NAME).data().toString());
    dialog->setBirthYear(index.sibling(row, columns::BIRTH_YEAR).data().toInt());
    dialog->setCity(index.sibling(row, columns::CITY).data().toString());
    dialog->setPersonalMobilePhone(index.sibling(row, columns::PERSONAL_MOBILE_PHONE).data().toString());
    dialog->setFriendMobilePhone(index.sibling(row, columns::FRIEND_MOBILE_PHONE).data().toString());
    dialog->setEmail(index.sibling(row, columns::EMAIL).data().toString());
    dialog->setComment(index.sibling(row, columns::COMMENT).data().toString());
    dialog->setCreationDateTime(index.sibling(row, columns::CREATION_DATETIME).data().toDateTime());
    dialog->setTeamID(index.sibling(row, columns::TEAM_ID).data().toInt());
    dialog->setRating(index.sibling(row, columns::RATING).data().toInt());

}

void TableModelParticipant::applyDialog(ParticipantDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::FIRST_NAME),dialog->getFirstName());
    setData(this->index(row, columns::SECOND_NAME),dialog->getSecondName());
    setData(this->index(row, columns::THIRD_NAME),dialog->getThirdName());
    setData(this->index(row, columns::BIRTH_YEAR),dialog->getBirthYear());
    setData(this->index(row, columns::CITY),dialog->getCity());
    setData(this->index(row, columns::PERSONAL_MOBILE_PHONE),dialog->getPersonalMobilePhone());
    setData(this->index(row, columns::FRIEND_MOBILE_PHONE),dialog->getFriendMobilePhone());
    setData(this->index(row, columns::EMAIL),dialog->getEmail());
    setData(this->index(row, columns::COMMENT),dialog->getComment());
    setData(this->index(row, columns::CREATION_DATETIME),dialog->getCreationDateTime());
    setData(this->index(row, columns::TEAM_ID),dialog->getTeamID());

    this->submitAll();
}


