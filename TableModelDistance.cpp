#include <QtSql/QSqlTableModel>
#include <QtCore/QDir>
#include <QtWidgets/QFileDialog>
#include "TableModelDistance.h"
#include "QTime"
#include "QDebug"

TableModelDistance::TableModelDistance(QObject *parent, const QSqlDatabase &db, const QString dbTable) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

void TableModelDistance::initDialog(DistanceDialog *dialog,QModelIndex index) {
    int row = index.row();
    dialog->setID(index.sibling(row, columns::ID).data().toString());
    dialog->setName(index.sibling(row, columns::NAME).data().toString());
    dialog->setNumberFrom(index.sibling(row, columns::NUMBER_FROM).data().toString());
    dialog->setNumberMax(index.sibling(row, columns::NUMBER_MAX).data().toString());
    dialog->setLapLimit(index.sibling(row, columns::LAP_LIMIT).data().toString());
    dialog->setTimeLimit(index.sibling(row, columns::TIME_LIMIT).data().toInt());
    dialog->setLapLength(index.sibling(row, columns::LAP_LENGTH).data().toString());
    dialog->setLapClimb(index.sibling(row, columns::LAP_CLIMB).data().toString());
    dialog->setComment(index.sibling(row, columns::COMMENT).data().toString());
}

void TableModelDistance::applyDialog(DistanceDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::COMMENT),dialog->getComment());
    setData(this->index(row, columns::NAME), dialog->getName());
    setData(this->index(row, columns::NUMBER_FROM), dialog->getNumberFrom());
    setData(this->index(row, columns::NUMBER_MAX), dialog->getNumberMax());
    setData(this->index(row, columns::LAP_LIMIT), dialog->getLapLimit());
    setData(this->index(row, columns::TIME_LIMIT), dialog->getTimeLimit());
    setData(this->index(row, columns::LAP_LENGTH), dialog->getLapLength());
    setData(this->index(row, columns::LAP_CLIMB), dialog->getLapClimb());
    this->submitAll();
}


