#pragma once

#include <QDialog>

namespace Ui {
    class TeamDialog;
}

class TeamDialog : public QDialog {
Q_OBJECT

public:
    explicit TeamDialog(QWidget *parent = nullptr);

    Ui::TeamDialog *getUI();

    ~TeamDialog();

    Ui::TeamDialog *ui;

private:
public:
    QString getName();

    void setName(QString name);

    QString getCity();

    void setCity(QString city);

    QString getId();

    void setId(QString id);

};