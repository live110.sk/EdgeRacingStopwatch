#include "teamdialog.h"
#include "ui_teamdialog.h"

TeamDialog::TeamDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TeamDialog)
{
    ui->setupUi(this);
}

TeamDialog::~TeamDialog()
{
    delete ui;
}

Ui::TeamDialog * TeamDialog::getUI() {
    return ui;
}

 QString TeamDialog::getName()  {
    return ui->lineEdit_teamName->text();
}

void TeamDialog::setName( QString name) {
    ui->lineEdit_teamName->setText(name);
}

 QString TeamDialog::getCity()  {
     return ui->lineEdit_cityName->text();
}

void TeamDialog::setCity( QString city) {
    ui->lineEdit_cityName->setText(city);
}

 QString TeamDialog::getId()  {
    return ui->lineEdit_id->text();
}

void TeamDialog::setId( QString id) {
    ui->lineEdit_id->setText(id);
}
