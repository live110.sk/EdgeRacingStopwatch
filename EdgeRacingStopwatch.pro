#-------------------------------------------------
#
# Project created by QtCreator 2018-08-27T23:35:09
#
#-------------------------------------------------

QT       += core gui widgets network sql xml printsupport serialport

TARGET = EdgeRacingStopwatch
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    teamdialog.cpp \
    chipdialog.cpp \
    distancedialog.cpp \
    participantdialog.cpp \
    groupdialog.cpp \
    eventdialog.cpp \
    settingsdialog.cpp \
    TableModelChip.cpp\
TableModelDistance.cpp \
TableModelParticipant.cpp \
TableModelTeam.cpp \
TableModelGroup.cpp \



HEADERS += \
TableModelGroup.h \
TableModelTeam.h \
TableModelChip.h \
TableModelDistance.h \
TableModelParticipant.h \
        mainwindow.h \
    teamdialog.h \
    chipdialog.h \
    distancedialog.h \
    participantdialog.h \
    groupdialog.h \
    eventdialog.h \
    settingsdialog.h

TRANSLATIONS = EdgeRacingStopwatch_ru_RU.ts

FORMS += \
        mainwindow.ui \
    teamdialog.ui \
    chipdialog.ui \
    distancedialog.ui \
    settingsdialog.ui \
    participantdialog.ui \
    groupdialog.ui \
    eventdialog.ui



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    translations.qrc
