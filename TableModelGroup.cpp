#include <QtSql/QSqlTableModel>
#include <QtCore/QDir>
#include <QtWidgets/QFileDialog>
#include "TableModelGroup.h"
#include "QTime"
#include "QDebug"
#include "groupdialog.h"

TableModelGroup::TableModelGroup(QObject *parent, const QSqlDatabase &db, const QString dbTable) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

void TableModelGroup::initDialog(GroupDialog *dialog,QModelIndex index) {
    int row = index.row();
    dialog->setId(index.sibling(row, columns::ID).data().toInt());
    dialog->setName(index.sibling(row, columns::NAME).data().toString());
    dialog->setDistance(index.sibling(row, columns::DISTANCE).data().toString());
    dialog->setAgeMin(index.sibling(row, columns::AGE_MIN).data().toInt());
    dialog->setAgeMax(index.sibling(row, columns::AGE_MAX).data().toInt());
    dialog->setPrice(index.sibling(row, columns::PRICE).data().toInt());
    dialog->setFemale(index.sibling(row, columns::FEMALE).data().toBool());
    dialog->setMale(index.sibling(row, columns::MALE).data().toBool());
    dialog->setStartTime(index.sibling(row, columns::START_TIME).data().toDateTime());
    dialog->setComment(index.sibling(row, columns::COMMENT).data().toString());
}

void TableModelGroup::applyDialog(GroupDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::NAME),dialog->getName());
    setData(this->index(row, columns::AGE_MIN),dialog->getAgeMin());
    setData(this->index(row, columns::AGE_MAX),dialog->getAgeMax());
    setData(this->index(row, columns::PRICE),dialog->getPrice());
    setData(this->index(row, columns::FEMALE),dialog->isFemale());
    setData(this->index(row, columns::MALE),dialog->isMale());
    setData(this->index(row, columns::START_TIME),dialog->getStartTime());
    setData(this->index(row, columns::COMMENT),dialog->getComment());
    this->submitAll();

}


