#pragma once


#include <QtCore/QString>
#include <QtCore/QTime>

class Distance {
public:
    const QString &getId() const;

    void setId(const QString &id);

    const QString &getName() const;

    void setName(const QString &name);

    const QString &getNumber_from() const;

    void setNumber_from(const QString &number_from);

    const QString &getNumber_max() const;

    void setNumber_max(const QString &number_max);

    const QString &getLaps_limit() const;

    void setLaps_limit(const QString &laps_limit);

    const QTime &getTime_limit() const;

    void setTime_limit(const QTime &time_limit);

    const QString &getLap_length() const;

    void setLap_length(const QString &lap_length);

    const QString &getLap_climb() const;

    void setLap_climb(const QString &lap_climb);

    const QDateTime &getStart_time() const;

    void setStart_time(const QDateTime &start_time);

    QString id;
QString name;
QString number_from;
QString number_max;
QString laps_limit;
QTime time_limit;
QString lap_length;
QString lap_climb;
QDateTime start_time;
};

