#pragma once


#include <QtSql/QSqlTableModel>
#include "distancedialog.h"
#include "teamdialog.h"

class TableModelTeam: public QSqlTableModel {
public:
    enum columns {
        ID = 0, NAME = 1, CITY = 2};
    TableModelTeam(QObject *parent, const QSqlDatabase &db,const QString dbTable);
    int findTeamCode(QString TeamCode);

    void initDialog(TeamDialog *dialog, QModelIndex index);
    void applyDialog(TeamDialog *dialog, QModelIndex index);
};