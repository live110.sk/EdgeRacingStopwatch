//
// Created by livello on 04.09.18.
//

#include <QtCore/QObject>
#include "DataBaseController.h"

DataBaseController::DataBaseController(QObject *parent) : QObject(parent) {

}

DataBaseController::~DataBaseController() {

}

/* Методы для подключения к базе данных
 * */
void DataBaseController::connectToDataBase() {
    /* Перед подключением к базе данных производим проверку на её существование.
     * В зависимости от результата производим открытие базы данных или её восстановление
     * */
    if (!QFile("C:/example/" DATABASE_NAME).exists()) {
        this->restoreDataBase();
    } else {
        this->openDataBase();
    }
}

/* Методы восстановления базы данных
 * */
bool DataBaseController::restoreDataBase() {
    if (this->openDataBase()) {
        if (!this->createTable()) {
            return false;
        } else {
            return true;
        }
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
    return false;
}

/* Метод для открытия базы данных
 * */
bool DataBaseController::openDataBase() {
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("storage.sqlite");
    if (!db.open()) {
        qCritical() << "Can not open storage.sqlite " << db.lastError().text();;
        return false;
    }
    qInfo() << tr("database opened ") << db.databaseName();
    return true;

}

/* Методы закрытия базы данных
 * */
void DataBaseController::closeDataBase() {
    db.close();
}

/* Метод для создания таблицы в базе данных
 * */
bool DataBaseController::createTable() {
    /* В данном случае используется формирование сырого SQL-запроса
     * с последующим его выполнением.
     * */
    QSqlQuery query;
    if (!query.exec("CREATE TABLE " TABLE " ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    TABLE_DATE      " DATE            NOT NULL,"
                    TABLE_TIME      " TIME            NOT NULL,"
                    TABLE_RANDOM    " INTEGER         NOT NULL,"
                    TABLE_MESSAGE   " VARCHAR(255)    NOT NULL"
                    " )"
    )) {
        qDebug() << "DataBase: error of create " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    return false;
}

/* Метод для вставки записи в базу данных
 * */
bool DataBaseController::inserIntoTable(const QVariantList &data) {
    /* Запрос SQL формируется из QVariantList,
     * в который передаются данные для вставки в таблицу.
     * */
    QSqlQuery query;
    /* В начале SQL запрос формируется с ключами,
     * которые потом связываются методом bindValue
     * для подстановки данных из QVariantList
     * */
    query.prepare("INSERT INTO " TABLE " ( " TABLE_DATE ", "
                  TABLE_TIME ", "
                  TABLE_RANDOM ", "
                  TABLE_MESSAGE " ) "
                  "VALUES (:Date, :Time, :Random, :Message )");
    query.bindValue(":Date", data[0].toDate());
    query.bindValue(":Time", data[1].toTime());
    query.bindValue(":Random", data[2].toInt());
    query.bindValue(":Message", data[3].toString());
    // После чего выполняется запросом методом exec()
    if (!query.exec()) {
        qDebug() << "error insert into " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
    return false;
}