#pragma once
#include <QDialog>

namespace Ui {
class GroupDialog;
}

class GroupDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GroupDialog(QWidget *parent = nullptr);
    ~GroupDialog();

private:
    Ui::GroupDialog *ui;
public:
     QString getName() ;

    void setName( QString name);

     QString getDistance() ;

    void setDistance( QString distance);

    int getAgeMin() ;

    void setAgeMin(int ageMin);

    int getAgeMax() ;

    void setAgeMax(int ageMax);

    bool isMale() ;

    void setMale(bool male);

    bool isFemale() ;

    void setFemale(bool female);

    int getPrice() ;

    void setPrice(int price);

    int getId() ;

    void setId(int id);

    void setStartTime(QDateTime time);

    void setComment(QString qString);
    QDateTime getStartTime();
    QString getComment();
};

