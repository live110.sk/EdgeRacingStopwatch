//
// Created by livello on 17.09.18.
//

#include "TableModelChip.h"
#include "QDebug"
#include "QtSql/QSqlRecord"
#include <QDateTime>


TableModelChip::TableModelChip(QObject *parent, const QSqlDatabase &db, const QString dbTable) : QSqlTableModel(parent, db) {
    QSqlTableModel(parent, db);
    setTable(dbTable);
    setEditStrategy(QSqlTableModel::OnManualSubmit);
    select();
}

int TableModelChip::findChipCode(QString chipCode) {
    for (int row = 0; row < rowCount(); row++) {
        QString tableChipCode = this->record(row).value("chip_code").toString();

        if (tableChipCode.compare(chipCode, Qt::CaseInsensitive) == 0) {
            return row;
        }
    }
    return -1;
}

void TableModelChip::initDialog(ChipDialog *dialog, QModelIndex index) {
    int row = index.row();
    dialog->setID(index.sibling(row, columns::ID).data().toString());
    dialog->setNumber(index.sibling(row, columns::NUMBER).data().toString());
    dialog->setCode(index.sibling(row, columns::CHIP_CODE).data().toString());
    dialog->setActive(index.sibling(row, columns::IS_ACTIVE).data().toBool());
    dialog->setCreated(index.sibling(row, columns::CREATION_DATETIME).data().toDateTime());
    dialog->setComment(index.sibling(row, columns::COMMENT).data().toString());

}

void TableModelChip::applyDialog(ChipDialog *dialog, QModelIndex index) {
    int row = index.row();
    setData(this->index(row, columns::NUMBER),dialog->getNumber());
    setData(this->index(row, columns::CHIP_CODE),dialog->getCode());
    setData(this->index(row, columns::IS_ACTIVE),dialog->getActive());
    setData(this->index(row, columns::CREATION_DATETIME),dialog->getCreated());
    setData(this->index(row, columns::COMMENT),dialog->getComment());
    submitAll();
}
