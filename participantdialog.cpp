#include "participantdialog.h"
#include "ui_participantdialog.h"
#include <QDebug>

ParticipantDialog::ParticipantDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ParticipantDialog)
{
    ui->setupUi(this);
    ui->lineEdit_rating->setReadOnly(true);
    ui->lineEdit_id->setReadOnly(true);
    ui->dateTimeEdit_creation->setReadOnly(true);
}

ParticipantDialog::~ParticipantDialog()
{
    delete ui;
}

int ParticipantDialog::getId()  {

    return ui->lineEdit_id->text().toInt();
}

void ParticipantDialog::setId(int id) {

    ui->lineEdit_id->setText(QString::number(id));
}

 QString ParticipantDialog::getFirstName()  {
    return ui->lineEdit_firstName->text();
}

void ParticipantDialog::setFirstName( QString firstName) {
    ui->lineEdit_firstName->setText(firstName);
}

 QString ParticipantDialog::getSecondName()  {
    return ui->lineEdit_secondName->text();
}

void ParticipantDialog::setSecondName( QString secondName) {
    ui->lineEdit_secondName->setText(secondName);
}

 QString ParticipantDialog::getThirdName()  {
    return ui->lineEdit_thirdName->text();
}

void ParticipantDialog::setThirdName( QString thirdName) {
    ui->lineEdit_thirdName->setText(thirdName);
}

 QString ParticipantDialog::getCity()  {
    return ui->lineEdit_city->text();
}

void ParticipantDialog::setCity( QString city) {
    ui->lineEdit_city->setText(city);
}

int ParticipantDialog::getBirthYear()  {
    return ui->spinBox_birthYear->value();
}

void ParticipantDialog::setBirthYear(int birthYear) {
    ui->spinBox_birthYear->setValue(birthYear);
}

 QString ParticipantDialog::getPersonalMobilePhone()  {
    return ui->lineEdit_mobile_phone->text();
}

void ParticipantDialog::setPersonalMobilePhone( QString personalMobilePhone) {
    ui->lineEdit_mobile_phone->setText(personalMobilePhone);
}

 QString ParticipantDialog::getFriendMobilePhone()  {
    return ui->lineEdit_friend_mobile_phone->text();
}

void ParticipantDialog::setFriendMobilePhone( QString friendMobilePhone) {
    ui->lineEdit_friend_mobile_phone->setText(friendMobilePhone);
}

 QString ParticipantDialog::getEmail()  {
    return ui->lineEdit_email->text();
}

void ParticipantDialog::setEmail( QString email) {
    ui->lineEdit_email->setText(email);
}

 QDateTime ParticipantDialog::getCreationDateTime()  {
    return ui->dateTimeEdit_creation->dateTime();
}

void ParticipantDialog::setCreationDateTime( QDateTime creationDateTime) {
    ui->dateTimeEdit_creation->setDateTime(creationDateTime);
}

int ParticipantDialog::getTeamID()  {
    return 1;
//    TODO: TeamId get
}

void ParticipantDialog::setTeamID(int teamID) {
//    TODO: TeamId set
}

int ParticipantDialog::getRating()  {
    return ui->lineEdit_rating->text().toInt();
}

void ParticipantDialog::setRating(int rating) {
    ui->lineEdit_rating->setText(QString(rating));
}

 QString ParticipantDialog::getComment()  {
    return ui->plainTextEdit_comment->toPlainText();
}

void ParticipantDialog::setComment( QString comment) {
    ui->plainTextEdit_comment->setPlainText(comment);
}
