#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QDebug>
#include <QPushButton>
#include <QtCore/QFile>
#include <QtCore/QDateTime>



int main(int argc, char *argv[]) {


    QApplication a(argc, argv);
    QTranslator translator;
    bool trans_load = translator.load(":/EdgeRacingStopwatch_ru_RU.qm");
    qDebug() << "load successfull=" << trans_load;
    a.installTranslator(&translator);
    MainWindow w;
    w.show();

    return a.exec();
}
