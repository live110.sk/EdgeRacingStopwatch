#pragma once
#include <QDialog>
#include "QDate"

namespace Ui {
class EventDialog;
}

class EventDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EventDialog(QWidget *parent = nullptr);
    ~EventDialog();

private:
    Ui::EventDialog *ui;
public:
    int getId() ;

    void setId(int id);

     QString getName() ;

    void setName( QString name);

     QDate getStart() ;

    void setStart( QDate start);

private:
    int id;
    QString name;
    QDate start;
};

