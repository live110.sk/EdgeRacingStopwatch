#pragma once


#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRelationalTableModel>
#include "groupdialog.h"

class TableModelGroup : public QSqlRelationalTableModel {
public:
    enum columns {
        ID = 0, NAME = 1, DISTANCE = 2, AGE_MIN = 3, AGE_MAX = 4, PRICE = 5, FEMALE = 6, MALE = 7, START_TIME = 8, COMMENT = 9
    };

    TableModelGroup(QObject *parent, const QSqlDatabase &db, const QString dbTable);
    void initDialog(GroupDialog *dialog, QModelIndex index);
    void applyDialog(GroupDialog *dialog, QModelIndex index);



};


