#include "chipdialog.h"
#include "ui_chipdialog.h"
#include "QCalendarWidget"

ChipDialog::ChipDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChipDialog)
{
    ui->setupUi(this);
}

ChipDialog::~ChipDialog()
{
    delete ui;
}
Ui::ChipDialog * ChipDialog::getUI(){
    return ui;
}

void ChipDialog::setID(QString value) {
    ui->lineEdit_id->setText(value);

}

QString ChipDialog::getNumber() {
    return ui->lineEdit_number->text();
}

QString ChipDialog::getCode() {
    return ui->lineEdit_code->text();
}

bool ChipDialog::getActive() {
    return ui->checkBox_isActive->isChecked();
}

QDateTime ChipDialog::getCreated() {
    return ui->dateTimeEdit_created->dateTime();
}

QString ChipDialog::getComment() {
    return ui->plainTextEdit_comment->toPlainText();
}

void ChipDialog::setNumber(QString value) {
    ui->lineEdit_number->setText(value);
}
void ChipDialog::setCode(QString value) {
    ui->lineEdit_code->setText(value);
}
void ChipDialog::setActive(bool value) {
    ui->checkBox_isActive->setChecked(value);
}
void ChipDialog::setCreated(QDateTime value) {
    ui->dateTimeEdit_created->setDateTime(value);
}
void ChipDialog::setComment(QString value) {
    ui->plainTextEdit_comment->setPlainText(value);
}
