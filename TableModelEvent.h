#pragma once


#include <QtCore/QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include "eventdialog.h"

class TableModelEvent : public QSqlTableModel {
public:
    enum columns {
        ID = 0, NAME = 1, START_DATE = 2
    };

    TableModelEvent(QObject *parent, const QSqlDatabase &db, const QString dbTable);
    void initDialog(EventDialog *dialog, QModelIndex index);
    void applyDialog(EventDialog *dialog, QModelIndex index);
};


